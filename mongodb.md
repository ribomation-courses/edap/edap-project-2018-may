Mongo DB
====

Mongo is a NoSQL database based on storage of JSON documents.
It's very popular in modern business application systems, especially
for large systems, where fault tolerance (mongo replica sets) and
scalability (mongo shards) are paramount.

You do not need to master mongo for this project, it will more or less
be a black box storing json documents/objects. However, it's advisable
to have a brief understanding of what it is.
A good starting point for understanding NoSQL in general and Mongo in
particular is the articles
* [NoSQL Databases Explained](https://www.mongodb.com/nosql-explained)
* [What is MongoDB?](https://www.mongodb.com/what-is-mongodb)
* [Quackit - MongoDB Tutorial](https://www.quackit.com/mongodb/tutorial/)
* [MongoDB Manual](https://docs.mongodb.com/manual/)

Installation of the Server
====

You can install Mongo on both Windows and Linux. For the project, you
will need to install it on Ubuntu Linux, but if you just want to play around
with it, Installing on Windows might be an option.

To install MongoDB on Ubuntu Linux, follow the instructions at
* [Install MongoDB Community Edition on Ubuntu](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/)
* [MongoDB Download Center](https://www.mongodb.com/download-center#community)

You need to review the instructions above so you have an understanding of what
is happening. However, here is the quick set of command-line operations

First prepare the APT repo

    sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 2930ADAE8CAF5059EE73BB4B58712A2291FA4AD5

    echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.6 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.6.list

    sudo apt-get update

Now, you can install the latest version of mongo

    sudo apt-get install -y mongodb-org

After a successful installation, you can launch the server with

    sudo service mongod start

The server can be shutdown and/or restarted by

    sudo service mongod stop
    sudo service mongod restart

The server will be listening for a client connection at port 27017
and you can connect using the client by

    mongo

Type `help` within the client to see a list of commands.

There are GUI front-ends available, which probably will help you
interact with mongo, instead of using the CLI tool above.
* [MongoDB Compass Community](https://www.mongodb.com/download-center#compass)
* [Studio 3T Core](https://studio3t.com/download/)

Installation of the C/C++ Drivers
====

To interact with a mongo server, from a C++ program, you need to install and
compile the C and the C++17 drivers.

Follow the instructions at
* [Installing the MongoDB C++ Driver (mongocxx)](https://mongodb.github.io/mongo-cxx-driver/mongocxx-v3/installation/)
* [Installing the MongoDB C Driver](http://mongoc.org/libmongoc/current/installing.html)

Before you proceed with the step below, ensure you have read the instructions above.


Mongo C Drivers
----
In short, the steps for the C Driver are
* Download the latest version of the C driver (v 1.9.4)
  - https://github.com/mongodb/mongo-c-driver/releases
* Install the optional dependencies
  - `sudo apt-get install pkg-config libssl-dev libsasl2-dev`
* Unpack the *.tar.gz file and change into the directory
  - `tar xzf mongo-c-driver-1.9.4.tar.gz`
  - `cd mongo-c-driver-1.9.4`
* Setup the build script
  - `./configure --disable-automatic-init-and-cleanup`
* Build the package
  - `make`
* Install the C driver
  - `sudo make install`  

Mongo C++ Driver
----
The steps for the C++ driver are
* Download the latest version (3.2.0)
  - https://github.com/mongodb/mongo-cxx-driver/releases
* Unpack the *.tar.gz file and change into the build directory
  - `tar xzf r3.2.0.tar.gz`
  - `cd mongo-cxx-driver-r3.2.0/build`
* Run the cmake generator
  - `cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/usr/local -DCMAKE_PREFIX_PATH=/usr/local ..`
* Build the package
  - `make`
* Install the C++ driver
  - `sudo make install`

Quick Test
----
Test the driver installation by typing in the hello mongo program at Step 6 of the instructions
* https://mongodb.github.io/mongo-cxx-driver/mongocxx-v3/installation/#step-6-test-your-installation

Share you Experience
----
Don't forget to share your experience of installing mongo and its drivers
to the rest of the group.

Working with MongoDB
====

Acquire a brief understanding of how to connect and interact with a mongo
server. The tutorial from Quackit (_at the beginning of this page_) is a
good source.

At the very minimum, you need to import (fake) data using `mongoimport`
and check the precense of your imported data. For the latter you can use
the `mongo` client.

Mongo Import
----
Let's say you have created the file `products.json`with a JSON array of
product JSON objects. You can import it using

    mongoimport --db edap --collection products --jsonArray --file products.json

The command above will import the records into the collection `products` of
the database instance `edap`. You don't have to create any of these before,
they will be created if needed.

Mongo Client
----
After a successful import, you can connect with

    mongo

Then switch to the `edap` database

    use edap
    show collections

Finally, list all records

    db.products.find().pretty()

Mongo GUI Client
----
A more convenient user experience is to use a GUI front-end. There are many to choose from, if you just google around a bit. However, my favorit is `Studio 3T for MongoDB` (former MongoChef). Just use it for non-commercial use.
* [Studio 3T](https://studio3t.com/)
* [Features](https://studio3t.com/features/)

The simplest way to install for Ubuntu Linux is download the `*.tar.gz`
for Linux, unpack it and run its installer

    tar xvfz studio-3t-linux-x64.tar.gz
    chmod +x studio-3t-linux-x64.sh
    ./studio-3t-linux-x64.sh


