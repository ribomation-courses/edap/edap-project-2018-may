REST Web Services
====

Understanding REST
====
During the project period, there will be improvised lectures about what is
a REST web service and how to realize it in C++.

HTTPie
----
In the mean-time, you should install the `HTTPie` client and play around with
it accessing the _JSON PlaceHolder_ website.

* [HTTPie Client](https://httpie.org/)
* [HTTPie Documentation](https://httpie.org/doc)
* [HTTP Prompt Front-End - not needed, but fun](http://http-prompt.com/)

HTTPie is implemented in Python, which will be installed when you install HTTPie.

    sudo apt install httpie

JSON PlaceHolder Web
----
Browse to JSON PlaceHolder and look around.
* [JSON PlaceHolder](https://jsonplaceholder.typicode.com/)

Keep in mind that you don't change anything on the that server, despite
you run HTTP operations as POST, PUT and DELETE. It's all _fake_.

JSON Server (_Not Required_)
----
If you like the server above and want to have one for yourself to play with.
You should install json-server.

This locally installed server requires that you first install NodeJS,
which is server-side JavaScript.

* [NodeJS](ttps://nodejs.org/)
* [Installing NodeJS for Ubuntu](https://nodejs.org/en/download/package-manager/#debian-and-ubuntu-based-linux-distributions)
* [JSON Server](https://github.com/typicode/json-server)

Once you have `node` and `npm` installed you can install the server with

    sudo npm install -g json-server


REST WS Server (`ngrest`)
====

The objective of this project is to create an authenticated web service for
a bunch of products. There are many C++ libraries out there. Many are really
good but also complex and has a steep learning curve. For this project, we are
using a simpler tool, that provides a command-line script builds and runs a server.

* [`ngrest` @ GitHub](https://github.com/loentar/ngrest)
* [`ngrest` Documentation](https://github.com/loentar/ngrest/wiki)
* [`ngrest` Installation](https://github.com/loentar/ngrest/wiki/Installation-guide-with-screenshots)

The installation is performed using a installer script, as you can read about above. After you have successfully installed `ngrest`; get yourself familiar with the provided demo. There you can find a complete CRUD realization and
a test-suite using HTTPie. 

* [`ngrest` Books and Users Demo](./demo/)

Unpack the TAR GZIP file with

    cp books-demo.tar.gz path/to/my/projects
    cd path/to/my/projects
    tar xvfz books-demo.tar.gz

_N.B._ You are supposed to read the documentation to understand and figure out how to proceed.

