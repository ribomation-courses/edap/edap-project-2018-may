EDAP: Project, 2018 May
====

* Start: May 9, 2018
* End: June 14-15, 2018


Short description
====
Design and implement a small REST-WS based client-service micro-services system, using a NoSQL backend.

Preparations
====

* Create a project GitLab repo and invite your co-worker as a member, with role _Developer_
* Invite me (`jensriboe`) as a member with role _Reporter_
* Generate fake _Product_ data at [Mockaroo](https://mockaroo.com/)
* Generate fake _User_ data at [Mockaroo](https://mockaroo.com/)
* Install `MongoDB` and populate it with the fake data 
* Install the `MongoDB C/C++ driver` and write a small test program to understand how it works
* Install the `HTTPie` client and learn how to use it
* Install the `ngrest` C++ frame-work and learn how to use it
* Investigate the provided ngrest demo and learn how to build and launch it

Objective
====

* Design an authenticated REST-WS API C++ server that delivers JSON product data retrieved from a MongoDB server
* It should support all CRUD (Create Read Update Delete) operations, after
    successful authentication
* Document the server REST-WS API
* Design a REST-WS client that performs a CRUD suite of calls to the server
* All non-trivial code should have unit tests written in Google Test/Mock
* Write function tests for the server using shell-scripts and HTTPie.
* All source code should be documented using doxygen

More Readings
====
* [MongoDB Details](./mongodb.md)
* [REST-WS Details](./rest-ws.md)

More Information
====
More information about REST-WS, authentication etc, will be published later.

